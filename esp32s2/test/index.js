const { json, raw, text, urlencoded } = require('body-parser');
const cookieParser = require('cookie-parser');

var fs = require('fs');
var express = require('express');
var app = express();

// 接口显示页面
app.use('*', function (req, res, next) {
  console.log(req.baseUrl);
  next();
});

app.use(cookieParser());
app.use(json())
app.use(raw({ limit: "2100000kb" }))
app.use(text())
app.use(urlencoded({ extended: true }))

app.get('/', function (req, res, next) {
  console.log("on get");

  console.log("headers", req.headers);
  console.log("body", req.body);

  res.send('get ok!!')
  res.status(200).end();
})

app.post('/image', function (req, res, next) {
  console.log("on post");
  console.log("headers", req.headers);
  console.log("body", req.body);
  if (req.body instanceof Buffer) {
    fs.writeFile(`${new Date().getTime()}.jpeg`, req.body, function (err) {//用fs写入文件
      if (err) {
        console.log(err);
      } else {
        console.log('写入成功！');
      }
    });
    fs.writeFile(`test.jpeg`, req.body, function (err) {//用fs写入文件
      if (err) {
        console.log(err);
      } else {
        console.log('写入成功！');
      }
    });
  }
  res.send('post ok!!')
  res.status(200).end();
})

app.listen(8098, function () {
  console.log('app listening on port 8098!');
});

# lononCam 迷你监控
```
开发芯片: EPS32-S2
ESP-IDF: release/v4.2
采集图像: 800 * 600
图像格式: JPEG
传输方式: HTTP-POST
```

### 串口通信协议
```
管脚: 
    GPIO17------TX
    GPIO18------RX
```
```bash
# 执行查看设备通讯指令
python ./test/uart.py
```

### 开启图片接收服务器
图片会以jpeg格式保存
```bash
cd test             # 进入目录
npm install         # 安装库
node index.js       # 运行 nodejs 脚本
```

### 编译并烧录工程
   
```bash
# 设置目标并编译
idf.py set-target esp32s2 build
# 烧录 和 监视
idf.py flash -p [PORT] monitor
# 擦除固件flash内存
idf.py erase_flash
# 烧录 和 监视
idf.py flash monitor
idf.py monitor
idf.py erase_flash flash monitor
```
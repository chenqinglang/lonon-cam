#ifndef _include_h_
#define _include_h_

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"

#include "driver/uart.h"
#include "driver/gpio.h"

#include "esp_log.h"
#include "esp_err.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_heap_caps.h"
#include "esp_wifi.h"
#include "esp_http_client.h"
#include "esp_netif.h"
#include "esp_tls.h"
#include "esp_ota_ops.h"
#include "esp_https_ota.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "cam.h"
#include "ov2640.h"
#include "ov3660.h"
#include "sensor.h"

#include "sccb.h"
#include "jpeg.h"
#include "board.h"

#include "app_camera.h"
#include "demoks_ceye.h"
#include "app_uart.h"
#include "app_http_request.h"
#include "advanced_https_ota.h"

#include "event_bit.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "cJSON.h"
#include "define.h"
#include "login.h"
#include "mqtt_client.h"
#include "lonon_gpio.h"
#include "lonon_pir.h"
#include "lonon_mqtt.h"
#include "hmac_sha256.h"
#include "sha256.h"
#include "lonon_http.h"


#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_eth.h"
#include "protocol_examples_common.h"

#include <esp_http_server.h>

#include "lonon_json.h"


#endif 

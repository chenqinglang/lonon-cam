#ifndef _demoks_ceye_h_
#define _demoks_ceye_h_

#include "esp_err.h"

#define CAT_EYE_CMD_DEV_SEEK 0x01         /*>! 寻找设备 */
#define CAT_EYE_DEV_GET_NET_STATE 0x02    /*>! 获取网络状态 */
#define CAT_EYE_CMD_CHANGE_WIFI_INFO 0x03 /*>! 修改 wifi 信息 */
#define CAT_EYE_CMD_CHANGE_URL 0x04       /*>! 修改 url 信息 */
#define CAT_EYE_CMD_PHOTO_GRAPH 0x05      /*>! 抓拍 */
#define CAT_EYE_CMD_OTA 0x06              /*>! OTA */
#define CAT_EYE_CMD_CAMERA_SETUP 0x07     /*>! 摄像机参数设置 */

#define EXECUTE_TRUE 0x01    /*>! 执行成功 */
#define EXECUTE_FALSE 0x02   /*>! 执行失败 */
#define EXECUTE_TIMEOUT 0x03 /*>! 执行超时 */
#define EXECUTE_NOW 0x04     /*>! 执行中 */

#define SETUP_TURN_ON 0x01  /*>! 打开 */
#define SETUP_TUEN_OFF 0x02 /*>! 关闭 */

#define NET_STATE_GETTO_NULL 0x01   /*>! 未连接路由器 */
#define NET_STATE_GETTO_ROUTE 0x02  /*>! 已连接路由器 */
#define NET_STATE_GETTO_SERVER 0x03 /*>! 已连接服务器 */
#define NET_STATE_DEV_UNTYING 0x04  /*>! 设备解绑 */

#define LOCK_TO_NET 0x01 /*>! 门锁到网络 */
#define NET_TO_LOCK 0x02 /*>! 网络到门锁 */

#define URL_LENGTH_MAX 512

typedef uint16_t demoks_ceye_size;

typedef enum uint8_t
{
    DEMOKS_CEYE_HTTP_PHOTO_URL = 0, /**< 传图地址 */
    DEMOKS_CEYE_HTTP_OTA_URL,       /**< OTA地址 */
    DEMOKS_CEYE_URL_MAX,
    LONON_OTA_FIG                   /*OTA标志*/
} demoks_ceye_url_t;

typedef enum   
{
    START_OTA = 0,          /*需要ota*/
    NULL_OTA                /*无需ota*/
} lonon_ota_fig_t;

esp_err_t demoks_ceye_handle_data(uint8_t *data, demoks_ceye_size len);
esp_err_t demoks_ceye_init(void);
esp_err_t get_wifi_info(uint8_t *ssid, uint8_t *passwd, wifi_auth_mode_t *auth, uint8_t *channel);
esp_err_t set_wifi_info(char ssid[32], char passwd[64], wifi_auth_mode_t *auth, uint8_t *channel);
esp_err_t set_app_url(demoks_ceye_url_t url_type, char *url);
esp_err_t get_app_url(demoks_ceye_url_t url_type, char *url);
esp_err_t set_camera_setup(CAMERA_SETUP_t setup);
esp_err_t get_camera_setup(CAMERA_SETUP_t *setup);


esp_err_t set_ota_fig(demoks_ceye_url_t type, lonon_ota_fig_t fig);
esp_err_t get_ota_fig(demoks_ceye_url_t type, lonon_ota_fig_t *fig);





#endif
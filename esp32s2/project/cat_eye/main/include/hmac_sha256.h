/*
 * hmac-sha256.h
 * Copyright (C) 2017 Adrian Perez <aperez@igalia.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef HMAC_SHA256_H
#define HMAC_SHA256_H

#include <stddef.h>
#include <stdint.h>

#define HMAC_SHA256_DIGEST_SIZE 32  /* Same as SHA-256's output size. */

void
hmac_sha256_build (uint8_t out[HMAC_SHA256_DIGEST_SIZE],
             const uint8_t *data, unsigned char data_len,
             const uint8_t *key, unsigned char key_len);

#endif /* !HMAC_SHA256_H */

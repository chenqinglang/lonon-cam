#ifndef _define_h_
#define _define_h_

/*
    设备信息
*/
typedef struct
{
    int humidity;                   //湿度
    int light;                     //照明
    int light_intensity;            //光照
    char message[64];               //消息
    int motor;                     //电机
    int temperature;                //温度
    char firmware_version[8];       //固件版本
}tencent_shadow_device_info;



#endif


        
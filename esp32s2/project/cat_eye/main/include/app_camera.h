#ifndef _app_camera_h_
#define _app_camera_h_

typedef struct CAMERA_SETUP
{
    uint8_t Auto_Exposure;
    uint8_t Light_Mode;
    uint8_t Color_Saturation;
    uint8_t Brightness;
    uint8_t Contrast;
    uint8_t Special_Effects;
    uint8_t Color_Bar;
    uint8_t mode;
} CAMERA_SETUP_t;

void cam_task(void *arg);

#endif
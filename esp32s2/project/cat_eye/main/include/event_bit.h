#ifndef _event_bit_h_
#define _event_bit_h_

#include "include.h"

EventGroupHandle_t app_event_handle;
/** 在这里分配BIT 最多可以定义32个事件。 */
#define APP_EVENT_BIT_PHOTOGRAPH BIT0 /** 抓拍 */
#define APP_EVENT_BIT_WIFI_CONNECTED BIT1
#define APP_EVENT_BIT_WIFI_FAIL BIT2
#define APP_EVENT_BIT_ON_SET_WIFI_INFO BIT4
#define APP_EVENT_BIT_PHOTOGRAPH_SUCCESS BIT5 /** 抓拍传图成功 */
#define APP_EVENT_BIT_PHOTOGRAPH_FAIL BIT6    /** 抓拍传图失败 */
#define APP_EVENT_BIT_PHOTOGRAPH_QR_REC BIT7  /** 抓拍到QR识别 */
#endif
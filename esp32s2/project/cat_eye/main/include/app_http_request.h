#ifndef _app_http_request_h_
#define _app_http_request_h_

#include "esp_err.h"

void wifi_init_sta(void *pvParameters);
esp_err_t http_rest_image_jpeg(uint8_t *jpeg_data, size_t len);
#endif
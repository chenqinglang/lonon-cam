#ifndef _lonon_mqtt_h_
#define _lonon_mqtt_h_

/*
    mqtt任务队列消息
*/
typedef struct
{
    int typed;                      //类型  MQTT_EVENT_CONNECTED    MQTT_EVENT_DATA
    char topic[128];                //主题
    char message[128];              //消息
}mqtt_queue_msg_struct;

/*
    mqtt发送队列
*/
typedef struct
{
    esp_mqtt_client_handle_t client;        //mqtt实例
    char topic[128];                        //主题
    char message[256];                      //消息   
}mqtt_send_queue_struct;

/*

    函数功能：启动mqtt服务器
    函数名称：lonon_mqtt_app_start

*/
void lonon_mqtt_app_start(void);
/*

    函数功能：mqtt发送事件任务
    函数名字：mqtt_send_event_task

*/
void mqtt_send_event_task(void);
/*
    函数功能：获取设备名称
    备注：即mac地址
*/
char *get_esp32_mac_address(void);

#endif

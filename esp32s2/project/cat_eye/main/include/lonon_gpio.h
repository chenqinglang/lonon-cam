#ifndef _gpio_h_
#define _gpio_h_




#define GPIO_OUTPUT_PIN_SEL         ((1ULL << OV2640_REST) | (1ULL << OV2640_PWDN) | (1ULL << LED_NET) | (1ULL << LED_CAM))
#define OV2640_REST                 4
#define OV2640_PWDN                 5
#define LED_NET                     12
#define LED_CAM                     13

#define GPIO_INPUT_PIN_SEL          ((1ULL << SW1) | (1ULL << PIR))
#define SW1                         26
#define PIR                         1


void gpio_init(void);
void gpio_task_example(void* arg);




#define LED_NET_1                   gpio_set_level(LED_NET,1);
#define LED_NET_0                   gpio_set_level(LED_NET,0);

#define LED_CAM_1                   gpio_set_level(LED_CAM,1);
#define LED_CAM_0                   gpio_set_level(LED_CAM,0);

#define OV2640_REST_1               gpio_set_level(OV2640_REST,1);
#define OV2640_REST_0               gpio_set_level(OV2640_REST,0);

#define OV2640_PWDN_1               gpio_set_level(OV2640_PWDN,1);
#define OV2640_PWDN_0               gpio_set_level(OV2640_PWDN,0);


#endif














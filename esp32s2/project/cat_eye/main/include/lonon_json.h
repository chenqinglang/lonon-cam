#ifndef _lonon_json_h_
#define _lonon_json_h_

/*
    函数功能：构建json格式影子数据字符串
    函数名字：build_shadow_string
    输出：*out_string       输出无格式json字符串
    输入：len              *out_string缓存长度
    返回：成功  true    失败    false
*/
int build_shadow_string(char *out_string,int len);
/*
    函数功能：构建json格式事件数据
    函数名字：build_event_string
    输出：*out_string       输出无格式json字符串
    输入：len              *out_string缓存长度
    返回：成功  true    失败    false
*/
int build_event_string(char *out_string,int len,char *key,int value);




#endif


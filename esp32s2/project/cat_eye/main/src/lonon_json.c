#include "include.h"

/*
    当前设备状态
*/
extern tencent_shadow_device_info esp32_state;

/*
    函数功能：构建json格式影子数据字符串
    函数名字：build_shadow_string
    输出：*out_string       输出无格式json字符串
    输入：len              *out_string缓存长度
    返回：成功  true    失败    false
*/
int build_shadow_string(char *out_string,int len)
{
    char *cjson_str = NULL;

    cJSON *json_shadow =  cJSON_CreateObject();
    cJSON *json_state =  cJSON_CreateObject();
    cJSON *json_reported =  cJSON_CreateObject();

    cJSON_AddItemToObject(json_shadow, "type", cJSON_CreateString("update"));
    cJSON_AddItemToObject(json_shadow, "version", cJSON_CreateNumber(0));
    cJSON_AddItemToObject(json_shadow, "clientToken", cJSON_CreateString("clientToken"));

    cJSON_AddItemToObject(json_reported, "humidity", cJSON_CreateNumber(esp32_state.humidity));
    cJSON_AddItemToObject(json_reported, "light", cJSON_CreateNumber(esp32_state.light));
    cJSON_AddItemToObject(json_reported, "light_intensity", cJSON_CreateNumber(esp32_state.light_intensity));
    cJSON_AddItemToObject(json_reported, "message", cJSON_CreateString(esp32_state.message));
    cJSON_AddItemToObject(json_reported, "motor", cJSON_CreateNumber(esp32_state.motor));
    cJSON_AddItemToObject(json_reported, "temperature", cJSON_CreateNumber(esp32_state.temperature));
    cJSON_AddItemToObject(json_reported, "firmware_version", cJSON_CreateString(esp32_state.firmware_version));
    
    cJSON_AddNullToObject(json_state, "desired");
    cJSON_AddItemToObject(json_state, "reported", json_reported);

    cJSON_AddItemToObject(json_shadow, "state", json_state);

    cjson_str = cJSON_Print(json_shadow);
    printf("build json:\n%s\n", cjson_str);

    cjson_str = cJSON_PrintUnformatted(json_shadow);

    if(strlen(cjson_str) >= len)
    {
        printf("build unformatted json to long! len:%d buf len:%d\n",strlen(cjson_str),len);
        cJSON_Delete(json_shadow);
        return false;
    }

    printf("build unformatted json len:%d\n", strlen(cjson_str));

    strcpy(out_string,cjson_str);
    cJSON_Delete(json_shadow);

    return true;
}


/*
    函数功能：构建json格式事件数据
    函数名字：build_event_string
    输出：*out_string       输出无格式json字符串
    输入：len              *out_string缓存长度
    返回：成功  true    失败    false
*/
int build_event_string(char *out_string,int len,char *key,int value)
{
    char *cjson_str = NULL;

    cJSON *json_shadow =  cJSON_CreateObject();

    cJSON_AddItemToObject(json_shadow, "event", cJSON_CreateString("simple"));
    cJSON_AddItemToObject(json_shadow, key, cJSON_CreateNumber(value));

    cjson_str = cJSON_Print(json_shadow);
    printf("build json:\n%s\n", cjson_str);

    cjson_str = cJSON_PrintUnformatted(json_shadow);

    if(strlen(cjson_str) >= len)
    {
        printf("build unformatted json to long! len:%d buf len:%d\n",strlen(cjson_str),len);
        cJSON_Delete(json_shadow);
        return false;
    }

    printf("build unformatted json len:%d\n", strlen(cjson_str));

    strcpy(out_string,cjson_str);
    cJSON_Delete(json_shadow);

    return true;
}


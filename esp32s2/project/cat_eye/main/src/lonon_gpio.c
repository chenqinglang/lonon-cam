#include "include.h"

#define ESP_INTR_FLAG_DEFAULT 0

xQueueHandle sw1_gpio_evt_queue = NULL;
xQueueHandle pir_gpio_evt_queue = NULL;

static void IRAM_ATTR sw1_gpio_isr_handler(void* arg)
{
    uint32_t gpio_num = (uint32_t) arg;

    xQueueSendFromISR(sw1_gpio_evt_queue, &gpio_num, NULL);
}

static void IRAM_ATTR pir_gpio_isr_handler(void* arg)
{
    uint32_t gpio_num = (uint32_t) arg;

    xQueueSendFromISR(pir_gpio_evt_queue, &gpio_num, NULL);
}

void sw1_task(void* arg)
{
    uint32_t io_num;
    unsigned char op_mode = 0;

    while(1) 
    {
        if(xQueueReceive(sw1_gpio_evt_queue, &io_num, portMAX_DELAY)) 
        {
            if(io_num == SW1)
            {
                vTaskDelay(50 / portTICK_PERIOD_MS);
                if(gpio_get_level(SW1) == 0)
                {
                    printf("[sw1_task] get sw1 falling edge\n");
                }
            }
        }
    }
}

void gpio_init()
{
    gpio_config_t io_conf;

    /*
        输出io配置
    */
   
    /*
        OV2640_REST OV2640_PWDN     
        LED_NET LED_CAM
    */
    //disable interrupt
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config(&io_conf);   

    OV2640_REST_1
    OV2640_PWDN_0
    LED_NET_1
    LED_CAM_1

    /*
        输入io配置（中断）
    */

    /*
        SW1
    */
    //disable interrupt
    io_conf.intr_type = GPIO_PIN_INTR_NEGEDGE;
    //set as output mode
    io_conf.mode = GPIO_MODE_INPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = (1ULL << SW1);
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 1;
    //configure GPIO with the given settings
    gpio_config(&io_conf); 
    
    /*
        PIR
    */
    //disable interrupt
    io_conf.intr_type = GPIO_PIN_INTR_POSEDGE;
    //set as output mode
    io_conf.mode = GPIO_MODE_INPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = (1ULL << PIR);
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config(&io_conf); 

    //create a queue to handle gpio event from isr
    sw1_gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));
    //create a queue to handle gpio event from isr
    pir_gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));

    //start gpio task
    xTaskCreate(sw1_task, "sw1_task", 2048, NULL, 5, NULL);
     //start gpio task
    xTaskCreate(pir_task, "pir_task", 2048, NULL, 5, NULL);

    //install gpio isr service
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);

    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(SW1, sw1_gpio_isr_handler, (void*) SW1);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(PIR, pir_gpio_isr_handler, (void*) PIR);
}










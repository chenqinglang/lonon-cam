#include "include.h"
/**
 * This is an example which echos any data it receives on UART1 back to the sender,
 * with hardware flow control turned off. It does not use UART driver event queue.
 *
 * - Port: UART1
 * - Receive (Rx) buffer: on
 * - Transmit (Tx) buffer: off
 * - Flow control: off
 * - Event queue: off
 * - Pin assignment: see defines below
 */

#define ECHO_TEST_TXD (GPIO_NUM_17)
#define ECHO_TEST_RXD (GPIO_NUM_18)
#define ECHO_TEST_RTS (UART_PIN_NO_CHANGE)
#define ECHO_TEST_CTS (UART_PIN_NO_CHANGE)

#define BUF_SIZE (1024)

extern EventGroupHandle_t app_event_handle;
static const char *TAG = "app_uart";

static uint8_t uart_data[512] = {0};
static demoks_ceye_size uart_data_len = 0;
static bool waiting = false;

static void check_uart_data()
{
    if (uart_data_len >= 7)
    {
        if (uart_data[0] == 0x57 && uart_data[1] == 0x72)
        {
            const uint16_t len = uart_data[4] * 0x100 + uart_data[5];

            if (uart_data_len >= 7 + len)
            {
                const uint8_t checksum = uart_data[6 + len];
                uint8_t checksum_tmp = 0;

                for (size_t i = 0; i < 6 + len; i++)

                    checksum_tmp += uart_data[i];

                if ((checksum_tmp & 0xFF) == checksum)
                {
                    ESP_LOGI(TAG, "校验通过");
                    demoks_ceye_handle_data(&uart_data, uart_data_len);
                    waiting = false;
                }
                else
                {
                    ESP_LOGW(TAG, "校验不通过, %02X", checksum);
                    waiting = false;
                }
            }
        }
        else
        {
            waiting = false;
            ESP_LOGW(TAG, "数据头不通过");
        }
    }
}

static void set_uart_data(uint8_t *data, size_t len)
{
    // 判断是否等待更多数据
    if (waiting)
    {
        for (size_t i = 0; i < len; i++)
        {
            uart_data[uart_data_len] = data[i];
            uart_data_len++;
            check_uart_data();
        }
    }
    else
    {
        if (len >= 2)
        {
            for (size_t i = 0; i < len; i++)
            {
                if (data[i] == 0x57 && data[i + 1] == 0x72)
                {
                    // 清空数据
                    memset(&uart_data, 0, sizeof(uart_data));
                    uart_data_len = 0;
                    waiting = true;
                }
                if (waiting)
                {

                    uart_data[uart_data_len] = data[i];
                    uart_data_len++;
                    check_uart_data();
                }
            }
        }
    }
}

void echo_task(void *arg)
{
    /* Configure parameters of an UART driver,
     * communication pins and install the driver */
    uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };
    uart_driver_install(UART_NUM_1, BUF_SIZE * 2, 0, 0, NULL, 0);
    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, ECHO_TEST_TXD, ECHO_TEST_RXD, ECHO_TEST_RTS, ECHO_TEST_CTS);

    // Configure a temporary buffer for the incoming data
    uint8_t *data = (uint8_t *)malloc(BUF_SIZE);

    while (1)
    {
        // Read data from the UART
        int len = uart_read_bytes(UART_NUM_1, data, BUF_SIZE, 20 / portTICK_RATE_MS);

        if (len > 0)
            set_uart_data(data, len);
    }
}
#include "include.h"

extern xQueueHandle pir_gpio_evt_queue;

uint32_t pir_backup_count = 0;

void pir_task(void* arg)
{
    uint32_t io_num,i;
    while(1) {
        if(xQueueReceive(pir_gpio_evt_queue, &io_num, portMAX_DELAY)) {
            if(io_num == PIR)
            {
                printf("[pir_task] get pir rising edge, now time is %d s\n",xTaskGetTickCount() / 100);

                if(xTaskGetTickCount() / 100 >= pir_backup_count + 10)
                {
                    /*
                        拍摄间隔10sec
                    */
                    printf("[pir_task] take photo\n");
                    /*>! 触发抓拍 */
                    xEventGroupSetBits(app_event_handle, APP_EVENT_BIT_PHOTOGRAPH);      
                    pir_backup_count = xTaskGetTickCount() / 100;           
                }           
            }
        }
    }
}

// void pir_task(void* arg)
// {
//     uint32_t io_num,i = 1;

//     vTaskDelay(1000 / portTICK_PERIOD_MS);
//     vTaskDelay(1000 / portTICK_PERIOD_MS);
//     vTaskDelay(1000 / portTICK_PERIOD_MS);
//     vTaskDelay(1000 / portTICK_PERIOD_MS);
//     vTaskDelay(1000 / portTICK_PERIOD_MS);
//     vTaskDelay(1000 / portTICK_PERIOD_MS);

//     /*   调试频繁拍照复位   */
//     while(1) {

//         /*
//             拍摄间隔10sec
//         */
//         printf("[pir_task] [debug test] take photo,cnt = %u\n",i ++);
//         /*>! 触发抓拍 */
//         xEventGroupSetBits(app_event_handle, APP_EVENT_BIT_PHOTOGRAPH);      
//         pir_backup_count = xTaskGetTickCount() / 100;   

//         vTaskDelay(1000 / portTICK_PERIOD_MS);
//         vTaskDelay(1000 / portTICK_PERIOD_MS);
//         vTaskDelay(1000 / portTICK_PERIOD_MS);
//         vTaskDelay(1000 / portTICK_PERIOD_MS);
//         vTaskDelay(1000 / portTICK_PERIOD_MS);
//         vTaskDelay(1000 / portTICK_PERIOD_MS);
//     }
// }

#include "include.h"

extern EventGroupHandle_t app_event_handle;
static const char *TAG = "app_main";

#define TENCENT_HTTP_URL            "http://lononcam-7gzsafx250a9853b-1304353880.ap-shanghai.app.tcloudbase.com/image"

/*
    当前设备状态
*/
tencent_shadow_device_info esp32_state;

static void app_init(void)
{
    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    ESP_ERROR_CHECK(demoks_ceye_init());

    app_event_handle = xEventGroupCreate();

    /*
        初始化设备信息
    */
    esp32_state.humidity = 10;
    esp32_state.light = 1;
    esp32_state.light_intensity = 56;
    strcpy(esp32_state.message,"hello esp-idf!");
    esp32_state.motor = 0;
    esp32_state.temperature = 32;
    strcpy(esp32_state.firmware_version,"1.0.2");

    //开机延时运行
    vTaskDelay(300 / portTICK_PERIOD_MS);

    //io配置
    gpio_init();    
    
    // 抓拍
    xEventGroupClearBits(app_event_handle, APP_EVENT_BIT_PHOTOGRAPH);
    xEventGroupClearBits(app_event_handle, APP_EVENT_BIT_ON_SET_WIFI_INFO);

    //初始化路由信息
    wifi_auth_mode_t auth = WIFI_AUTH_WPA2_PSK;
    
    //初始化服务器信息
    char url_string[128];
    memset(url_string,0,sizeof(url_string));

    sprintf(url_string,"%s?mac=%s",TENCENT_HTTP_URL,get_esp32_mac_address());

    set_app_url(
        DEMOKS_CEYE_HTTP_OTA_URL,
        "https://api.demoks.com/ota/cctv.bin"
    );
    
    set_app_url(
        DEMOKS_CEYE_HTTP_PHOTO_URL,
        url_string
    );

    char string[256];
    int len;

    unsigned char fig;
    /*检查会否需要ota*/
    ESP_LOGI(TAG,"检查ota标志");
    if(get_ota_fig(LONON_OTA_FIG,&fig) == ESP_OK)
    {
        ESP_LOGI(TAG,"获取到ota标志");
        /*需要ota*/
        if(fig == START_OTA)
        {
            ESP_LOGI(TAG,"等待联网后启动固件升级");
            xTaskCreate(wifi_init_sta, "wifi_init_sta", 2048 * 2, NULL, 5, NULL);

            /** 联网 */
            EventBits_t uxBits = xEventGroupWaitBits(app_event_handle,
                                APP_EVENT_BIT_WIFI_CONNECTED | APP_EVENT_BIT_WIFI_FAIL,
                                pdTRUE,
                                pdFALSE,
                                (portTickType)portMAX_DELAY);

            if(uxBits == APP_EVENT_BIT_WIFI_FAIL)
            {
                /*联网失败*/
                set_ota_fig(LONON_OTA_FIG,NULL_OTA);
                ESP_LOGI(TAG,"联网失败，重启");
                vTaskDelay(1000 / portTICK_PERIOD_MS);
                esp_restart();
            }
            
            ESP_LOGI(TAG,"联网成功，开始固件升级");
            if (advanced_https_ota() == ESP_OK)
            {
                set_ota_fig(LONON_OTA_FIG,NULL_OTA);
                ESP_LOGI(TAG,"固件升级成功，重启");
                vTaskDelay(1000 / portTICK_PERIOD_MS);
                esp_restart();
            }
            else
            {
                set_ota_fig(LONON_OTA_FIG,NULL_OTA);
                ESP_LOGI(TAG,"固件升级失败，重启");
                vTaskDelay(1000 / portTICK_PERIOD_MS);
                esp_restart();
            }
        }
        else
        {
            ESP_LOGI(TAG,"无需固件升级");
        }
        
    }
}

void reset_task(void *arg)
{
    unsigned int count = 0;

    /*
        每一小时重启一次
    */
    for(count = 0;count < (60 * 60 * 1);count ++)
        vTaskDelay(1000 / portTICK_PERIOD_MS);

    ESP_LOGI(TAG,"计时结束，重启");
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    esp_restart();
}

void app_main(void)
{
    ESP_LOGI(TAG, "[free mem good=%d;] \n", esp_get_free_heap_size());
    app_init();
    
    xTaskCreate(cam_task, "cam_task", 1024 * 16, NULL, configMAX_PRIORITIES, NULL);
    xTaskCreate(wifi_init_sta, "wifi_init_sta", 2048 * 2, NULL, 5, NULL);
    xTaskCreate(reset_task, "reset_task", 512, NULL, 5, NULL);
    lonon_mqtt_app_start();
}







# lononCam（迷你监控）
```
该项目是一个网络摄像机。主控使用esp32s2，前端使用微信小程序，小程序可以通过云函数使用mqtt下发指令进行抓拍，设备则通过http post照片到云函数转存到云存储同时写入照片事件到数据让小程序可以查看抓拍事件。
```
![image](./doc/image/image(1).jpg)
## Miniprogram qrcode format(binding device)
设备mac地址（小写）作为设备名称，md5生成规则为 "langshao" + 设备名称，示例：langshao123456abcdef，md5取头两字节数据
```json
{
  "device_name":"123456abcdef",
  "md5":"xxxx"
}
```
## Database

| 序号 | 集合名       | 说明              | 权限设置                                   |
| ---- | ----------- | ----------------  | ------------------------------------------|
| 1    | event       | 事件数据          | 仅创建者及管理员可读写                       |
| 2    | device      | 设备信息          | "read": true,"write": true,"delete": true  |
| 3    | firmware    | 固件信息          | 所有用户可读，仅管理员可写                   |
## Database format(device)
```json
{
  "_id":"xxx",
  "_openid":"o7gKT4ojN3Rj1gPM7qZkR_3g33FQ",
  "device_name":["0":"7cdfa101f672","1":"7cdfa101f620","2":"7cdfa101f8ac"]
}
```
## Database format(event)
```json
{
  "_id":"xxx",
  "device_name":"7cdfa101f8ac",
  "event":"photo",
  "event_data":"1636172099000(7cdfa101f8ac).jpg",
  "event_time":1636172099000
}
```
## Database format(firmware)
```json
{
  "_id":"xxx",
  "newest_version":"1.0.2"
}
```
## Cloud function
| 序号 | 集合名                  | 说明                                          | 
| ---- | ---------------------- | --------------------------------------------  | 
| 1    | iothub_get_state       |  查询设备是否在在线                             |
| 2    | iothub-publish         |  触发固件升级                                  | 
| 3    | iothub-shadow-query    |  请求影子数据                                  | 
| 4    | iothub_clean           | 定时清理云储存照片，防止超出容量                 | 
| 5    | iothub_image           | htts推送照片，转存云储存与创建照片日志写入数据库  |
## Cloud storage
```
image/                    存储照片
```
## .gitignore

ca_cert.pem：需要提供ota服务器证书，login.h：需要提供mqtt登录信息
```cpp
#define TENCENT_MQTT_PRODUCT_ID "xxxx"//产品id
#define TENCENT_MQTT_DEVICE_NAME get_esp32_mac_address()//设备名称
#define TENCENT_MQTT_KEY "xxxx"//hmacsha256 密钥
#define TENCENT_MQTT_USERNAME_PART "xxxx"//登录用户名组件（前面需要插入产品di）
#define TENCENT_MQTT_URL_PART "xxxx"//服务器地址组件（%s需要传入产品id）
#define TENCENT_MQTT_PORT 1883//服务器端口
#define TENCENT_MQTT_PASSWORD_PART "xxxx"//服务器登录密码组件（前面插入hmacsha256密码）
#define TENCENT_SHADOW_TOPIC "xxxx"//影子数据主题
```
app.js：需要提供云开发平台访问密钥
```javascript
App({
  globalData: {
    productId: "xxxx", 
    deviceName: "", 
    secretId: "xxxx",
    secretKey: "xxxx",
  },
  onLaunch: function () {
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        env: "xxx",
        traceUser: true,
      })
    }
  }
})
```
## Device photo
![image](./doc/image/image(2).jpg)
![image](./doc/image/image(3).jpg)
![image](./doc/image/image(4).jpg)
## Miniprogram qrcode
![image](./doc/image/image(8).jpg)
## Miniprogram photo
![image](./doc/image/image(5).jpg)
![image](./doc/image/image(6).jpg)
![image](./doc/image/image(7).jpg)

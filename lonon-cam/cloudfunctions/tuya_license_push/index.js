// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext()
    var jsonstr = Buffer.from(event.body, 'base64')
    console.log("收到数据，解码base64", jsonstr.toString())
    const strToObj = JSON.parse(jsonstr)

    try {
        const db = cloud.database()
        var retlog
        var result

        result = await db.collection('tuya_license').where({
            file: strToObj.file
        }).get()

        if (result.data.length > 0) {
            retlog = "授权码文件已存在，不能重复提交"
            console.log(retlog)
            return retlog
        }

        result = await db.collection('tuya_license').add({
            data: strToObj
        })

        console.log(result)
        return result
    } catch (e) {
        console.log(e)
    }
    return event
}


const tencentcloud = require("tencentcloud-sdk-nodejs");

// 导入对应产品模块的client models。
const IotcloudClient = tencentcloud.iotcloud.v20180614.Client;
const models = tencentcloud.iotcloud.v20180614.Models;

const Credential = tencentcloud.common.Credential;
const ClientProfile = tencentcloud.common.ClientProfile;
const HttpProfile = tencentcloud.common.HttpProfile;

exports.main = async (event, context) => {
  // 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey
  let cred = new Credential(event.SecretId, event.SecretKey); 

  // 实例化一个http选项，可选的，没有特殊需求可以跳过。
  let httpProfile = new HttpProfile();
  httpProfile.endpoint = "iotcloud.tencentcloudapi.com";  // 指定接入地域域名(默认就近接入)

  // 实例化一个client选项，可选的，没有特殊需求可以跳过。
  let clientProfile = new ClientProfile();  
  clientProfile.httpProfile = httpProfile;

  // 实例化要请求产品(以cvm为例)的client对象。clientProfile可选。
  let client = new IotcloudClient(cred, "ap-guangzhou", clientProfile);

  // 实例化一个请求对象,并填充参数
  let req = new models.DescribeDeviceShadowRequest(); 
  req.DeviceName = event.DeviceName;  //设置设备名称
  req.ProductId = event.ProductId;    //设置产品id

  //resolve	查询的结果，Result 定义见下方
  //reject	失败原因
  return new Promise((resolve, reject)=>{
    // 通过client对象调用想要访问的接口，需要传入请求对象以及响应回调函数
    client.DescribeDeviceShadow(req, function (errMsg, response) {
      // 请求异常返回，打印异常信息
      if (errMsg) {
        console.log(errMsg);
        reject(errMsg)
        return;
      }
      // 请求正常返回，打印response对象
      console.log(response.to_json_string());
      resolve(response)
    });
  })
}

// miniprogram/pages/Share/Share.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  /**
   * 时间戳转日期
   */
  getLocalTime(nS) {     
    return new Date(parseInt(nS)).toLocaleString().replace(/:\d{1,2}$/,' ');     
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("[onLoad]", options)

    if(options.image)
    {
      /**
       * 展示图片时间
       */
      this.setData({
        view_title_text: this.getLocalTime(options.image.substring(0,13)),
        view_title_device_text: '来自设备：' + options.device_name
      })
      
      /**
       * 展示图片
       */
      wx.showLoading()
      wx.cloud.downloadFile({
        fileID: "cloud://lononcam-7gzsafx250a9853b.6c6f-lononcam-7gzsafx250a9853b-1304353880/" + "image/" + options.image
      }).then(res => {
        wx.showToast({
          title: '下载成功',
        })
        console.log(res)
        this.setData({
          display_image: res.tempFilePath
        })
      }).catch(error => {
        console.log("[picker_bindChange][图片下载错误]",error)
        wx.showToast({
          title: '下载失败',
        })
      })  
    }
    else
    {
      /**
       * 无效分享
       */
      wx.showToast({
        title: '无效分享',
      })
    }
  },
  /**
   * 保存图片
   */
  save_image_button: function(e) {
    console.log("[save_image_button]")

    wx.saveImageToPhotosAlbum({
      filePath: this.data.display_image,
      success(res){
        console.log("[save_image_button][保存图片成功]")
        wx.showToast({
          title: '保存成功',
        }) 
      },
      fail(err){
        console.log("[save_image_button][保存图片失败]")
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
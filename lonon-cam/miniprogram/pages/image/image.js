const app = getApp()
const { $Message } = require('../../dist/base/index');

/**
 * 日志内容
 * event_data_list  存放日期展示用
 * event_data_list_time_stamp 存放时间戳下载文件用
 */
var event_data_list = []
var event_data_list_time_stamp = []
/**
 * 日志当前偏移位置
 */
var event_offset_num = 0
/**
 * 当前日志id
 */
var event_now_id
/**
 * 记录日志对象
 * 含_id 时间戳
 */
var obj = {}
/**
 * 记录当前文件名
 */
var current_file_name
/**
 * 记录当前选择的设备
 */
var now_set_dev = 0
/**
 * 监听数据库
 */
var watcher
var lis_fig = false

/**
 * 设备列表
 */
var lis = []
/**
 * 线上固件
 */
let firmware_str

Page({

  /**
   * 页面的初始数据
   */
  data: {
    view_title_text: 0,
    view_title_device_nametext: getApp().globalData.deviceName,
    picker_switch: false,
    view_title_num: "(0/0)",
    current: 'camera_key',
    camera_page_sw: true,
    device_page_sw: false,
    setup_page_sw: false,
    showRight1: false,
    ota_button_sw:  false,
  },
  /**
   * 时间戳转日期
   */
  getLocalTime(nS) {     
    return new Date(parseInt(nS)).toLocaleString().replace(/:\d{1,2}$/,' ');     
  },
  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad (options) {
    console.log("[onLoad]")

    event_data_list.length = 0
    event_data_list_time_stamp.length = 0
    event_offset_num = 0
    event_now_id = ""
    obj = {}
    lis = []

    /**
     * 清空事件列表渲染
     */
    this.setData({
      event_choice: event_data_list
    })
    
    /**
     * 查询设备名称
     */
    const db = wx.cloud.database()
    
    try{
      const res0 = await db.collection('device').where({
        _openid: this.data.openid
      }).get()
      console.log("[onLoad][获取设备名称][成功]: ", res0.data[0].device_name[0])
      getApp().globalData.deviceName = res0.data[0].device_name[now_set_dev]
      console.log("[onLoad]deviceName: ", getApp().globalData.deviceName)
      this.setData({
        view_title_device_nametext: getApp().globalData.deviceName
      })
      
      /**
       * device页展示
       */
      var now_dev
      for(var i = 0;i < res0.data[0].device_name.length;i ++)
      {
        if(now_set_dev == i)
        now_dev = {title:"设备：" + res0.data[0].device_name[i] + '（当前设备）'}
        else
        now_dev = {title:"设备：" + res0.data[0].device_name[i]}

        lis.push(now_dev)
      }

      if(res0.data[0].device_name.length != 0)
      {
        console.log("请求影子数据")
        wx.cloud.callFunction({
          name: 'iothub-shadow-query', 
          data: {  
            ProductId: app.globalData.productId,
            DeviceName: app.globalData.deviceName,
            SecretId: app.globalData.secretId,
            SecretKey: app.globalData.secretKey,
          },
          success: res => {
            console.log(res)
            console.log("获取影子数据成功")
            console.log(res.result.Data)

            const shadow_data = JSON.parse(res.result.Data) 
            if(shadow_data)
            {
              console.log("解析影子数据成功")
              if(shadow_data.payload.state.reported.firmware_version)
              {
                console.log("固件版本：" + shadow_data.payload.state.reported.firmware_version)
                
                firmware_str = shadow_data.payload.state.reported.firmware_version
                this.setData({
                  firmware_version_title_text: firmware_str
                });
                
                const db = wx.cloud.database()
                db.collection('firmware').where({
                  _id: "1526e12a6013fb2c0151a1b407ef5eea",
                }).get({
                  success:  (res)=>{
                    console.log("线上最新版本固件：",res.data[0].newest_version) 
                    if(res.data[0].newest_version != firmware_str)
                    {
                      $Message({
                        content: '发现新固件：' + firmware_str,
                        duration: 3
                      });
                      this.setData({
                        ota_button_sw:  true
                      })
                    }
                    else{
                      this.setData({
                        ota_button_sw:  false
                      })
                    }
                  }
                })
              }
              else
              {
                this.setData({
                  firmware_version_title_text: "未知"
                });
              }
            }
            else
            {
              console.log("解析影子数据失败")
              this.setData({
                firmware_version_title_text: "未知"
              });
            }
          },
          fail: (err) =>{
            console.log(err)
            console.log("获取影子数据失败")
          }
        })
      }
      this.setData({
        device_list: lis
      });
      /**
       * 设置全局设备名称
       */   
    }catch(err){
      console.log("[onLoad][获取设备名称][失败]", err)
      return
    }
    
    /**
     * 监听数据库变化
     */
    if(lis_fig == true)
    {
      watcher.close()
    }

    this.listen_database_change()

    try{
      /**
       * 查询日志数量
       */
      const res1 = await db.collection('event').where({
        event: "photo",
        device_name: getApp().globalData.deviceName
      }).count()
      console.log("[onLoad][获取日志数量成功]：", res1.total)
      this.setData({
        view_title_text: res1.total
      })
      /**
       * 日志数量为0
       */
      if(res1.total == 0)
      {
        wx.showToast({
          title: '没有记录',
        })
        this.setData({
          picker_switch: false
        })
        return
      }

      this.setData({
        picker_switch: true
      })

      wx.showToast({
        title: '请求记录',
      })
      /**
       * 获取日志内容
       * 一次最多20条
       * 后续懒加载
       */
      const res2 = await db.collection('event')
      .skip(res1.total > 20 ? res1.total - 20 : 0)
      .where({
        event: "photo",
        device_name: getApp().globalData.deviceName
      }).get()
      console.log("[onLoad][获取日志内容成功]：",res2)
      console.log("[onLoad][返回日志数量]：",res2.data.length)

      /**
       * 备份当前便宜位置
       * 懒加载使用
       */
      event_offset_num = res1.total > 20 ? res1.total - 20 : 0
      
      /**
       * 日志内容
       * 时间戳转日期
       * 放入数组尾部
       */
      obj = {}
      for(var i = 0;i < res2.data.length;i ++)
      {
        event_data_list.push(this.getLocalTime(res2.data[i].event_data.substring(0,13)))

        obj = {}
        obj.time_stamp =  res2.data[i].event_data
        obj._id = res2.data[i]._id

        console.log('[for]',i,res2.data[i].event_data,res2.data[i]._id)

        event_data_list_time_stamp.push(obj)
      }
      
      /**
       * 输出变量便于调试
       */
      console.log('[输出变量便于调试][event_data_list]',event_data_list)
      console.log('[输出变量便于调试][event_data_list_time_stamp]',event_data_list_time_stamp)

      /**
       * 渲染界面
       */
      this.setData({
        event_choice: event_data_list
      })

      /**
       * 记录事件id，便于删除按钮操作
       * 记录最前id因为下面会滚动到最前
       */
      event_now_id = res2.data[res2.data.length - 1]._id
      console.log("[event_now_id]",event_now_id)

      /**
       * 记录文件名用于传递分享
       */
      current_file_name = res2.data[res2.data.length - 1].event_data
      console.log("[current_file_name]", current_file_name)

      /**
       * 滚动到最前
       */
      this.setData({
        value: [res2.data.length - 1]
      })

      /**
       * 请求图片
       */
      var file_name = event_data_list_time_stamp[event_data_list_time_stamp.length - 1].time_stamp
      console.log("[请求图片]",file_name)

      wx.showLoading()
      wx.cloud.downloadFile({
        fileID: "cloud://lononcam-7gzsafx250a9853b.6c6f-lononcam-7gzsafx250a9853b-1304353880/" +'image/' + file_name
      }).then(res => {
        wx.showToast({
          title: '下载成功',
        })
        console.log(res)
        this.setData({
          display_image: res.tempFilePath
        })
      }).catch(error => {
        console.log("[onLoad][图片下载错误]",error)
        wx.showToast({
          title: '下载失败',
        })
      })      
    }catch(err){
      console.log("[onLoad][日志操作错误]：", err)
      return
    }
  },
  /**
   * 日志选择条刷新
   */
  async picker_bindChange (e) {
    /**
     * 检查是否有效事件
     */
    if(event_data_list.length == 0)
    {
      console.log("[picker_bindChange][无效事件]")
      return 
    }
    /**
     * 记录文件名用于传递分享
     */
    current_file_name = event_data_list_time_stamp[e.detail.value[0]].time_stamp
    console.log("[current_file_name]", current_file_name)
    /**
     * 当前选择条目
     */
    console.log("[picker_bindChange]: ", e.detail.value[0])
    var file_name = event_data_list_time_stamp[e.detail.value[0]].time_stamp
  
    /**
     * 记录事件id，便于删除按钮操作
     * 记录最前id因为下面会滚动到最前
     */
    event_now_id = event_data_list_time_stamp[e.detail.value[0]]._id
    console.log("[event_now_id]",event_now_id)

    /**
     * 滚动到第一个事件
     * 启动懒加载
     */
    if(!e.detail.value[0] && event_offset_num)
    {
      console.log("[启动懒加载]")
      
      const db = wx.cloud.database()
      try{
        /**
         * 获取日志内容
         * 一次最多20条
         * 后续懒加载
         */
        
        /**
        * 本次操作的偏移
        */
        var event_offset_num_now
        /**
         * 本次获取日志
         * 有效条目数量
         */
        var now_get_eff

        /**
         * 获取本次需要偏移位置
         */
        if(event_offset_num > 20)
        {
          event_offset_num_now = event_offset_num - 20
          now_get_eff = 20
        }
        else
        {
          event_offset_num_now = 0
          now_get_eff = event_offset_num
        }
        
        const res2 = await db.collection('event')
        .skip(event_offset_num_now)
        .where({
          event: "photo",
          device_name: getApp().globalData.deviceName
        }).get()
        console.log("[获取日志内容成功]：",res2)
        console.log("[返回日志数量]：",res2.data.length)

        
        /**
         * 日志内容
         * 时间戳转日期
         * 放入数组头部
         * 备注：需要反转顺序插入
         */
        obj = {}
        for(var i = 0;i < now_get_eff;i ++)
        {
          event_data_list.unshift(this.getLocalTime(res2.data[(now_get_eff - 1) - i].event_data.substring(0,13)))

          obj = {}
          obj.time_stamp =  res2.data[(now_get_eff - 1) - i].event_data
          obj._id = res2.data[(now_get_eff - 1) - i]._id

          event_data_list_time_stamp.unshift(obj)
        }
        
        /**
         * 渲染界面
         */
        this.setData({
          event_choice: event_data_list
        })

        /**
         * 滚动到最前
         */
        this.setData({
          value: [event_offset_num - event_offset_num_now]
        })
        
        event_offset_num = event_offset_num_now

        /**
         * 请求图片
         */
        console.log("[请求图片]",file_name)

        wx.showLoading()
        wx.cloud.downloadFile({
          fileID: "cloud://lononcam-7gzsafx250a9853b.6c6f-lononcam-7gzsafx250a9853b-1304353880/" +'image/' + file_name
        }).then(res => {
          wx.showToast({
            title: '下载成功',
          })
          console.log(res)
          this.setData({
            display_image: res.tempFilePath
          })
        }).catch(error => {
          console.log("[picker_bindChange][图片下载错误]",error)
        })
      }catch(err){
        console.log("[懒加载错误]", err)
      }
    }
    else
    {
        /**
         * 请求图片
         */
        console.log("[请求图片]",file_name)
   
        wx.showLoading()
        wx.cloud.downloadFile({
          fileID: "cloud://lononcam-7gzsafx250a9853b.6c6f-lononcam-7gzsafx250a9853b-1304353880/" +'image/' + file_name
        }).then(res => {
          wx.showToast({
            title: '下载成功',
          })
          console.log(res)
          this.setData({
            display_image: res.tempFilePath
          })
        }).catch(error => {
          console.log("[picker_bindChange][图片下载错误]",error)
        })      
    }
  },
  /**
   * 设备配网
   */
  set_net_button: function(e){
    console.log("[set_net_button]")
    wx.navigateTo({
      url: '/pages/setnetwork/setnetwork',
    })
  },
  /**
   * 扫码绑定
   */
  async qrcode_button (e){
    console.log("[qrcode_button]")
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) =>{
        console.log(res)
        const qrcode_data = JSON.parse(res.result) 
        if(qrcode_data)
        {
          if(qrcode_data.device_name)
          {
            /**
             * md5规则
             * langshao  + 设备名称
             */
            if(qrcode_data.md5)
            {
              var utilMd5 = require('../utils/md5.js');  
              var password = utilMd5.hexMD5("langshao" + qrcode_data.device_name);
              console.log("password: ",password)
              console.log("qrcode md5: ",qrcode_data.md5)

              if(qrcode_data.md5 != password.substring(0,4))
              {
                console.log("[非法二维码]",)
                wx.showToast({
                  title: '非法二维码'
                })
                return
              }
              
              wx.showToast({
                title: '获取成功',
              })
              console.log("[qrcode_button][device_name]: ",qrcode_data.device_name)
              /**
               * 读取数据库设备名称信息
               */
              const db = wx.cloud.database()
              /**
               * 查询当前用户的 device
               */
              db.collection('device').where({
                _openid: this.data.openid
              }).get({
                success: res => {
                  console.log("[查询数据库集合][device][成功]: ",res)
                  /**
                   * 没有存储过需要新建
                   */
                  if(!res.data.length)
                  {
                    db.collection('device').add({
                      data: {
                        device_name: [qrcode_data.device_name]
                      },
                      success: res => {
                        console.log("[存储记录成功][device][成功]: ",res)
                        /**
                         * 刷新页面
                         */
                        this.onLoad()
                      },
                      fail: err => {
                        console.log("[存储记录失败][device][失败]: ",err)
                      }
                    })
                  }
                  else
                  {
                    var new_dev_array = []
                    new_dev_array = res.data[0].device_name

                    if(new_dev_array.length >= 5)
                    {
                      /**
                       *  设备已满
                       */
                      wx.showToast({
                        title: '设备已满',
                      })
                      return
                    }
                    
                    if(new_dev_array.includes(qrcode_data.device_name))
                    {
                      /**
                       * 设备已存在
                       */
                      wx.showToast({
                        title: '设备已存在',
                      })
                      return
                    }

                    new_dev_array.push(qrcode_data.device_name)
                    console.log("[new_dev_array]",new_dev_array)

                    db.collection('device').doc(res.data[0]._id).update({
                      data: {
                        device_name: new_dev_array
                      },
                      success: res => {
                        console.log("[更新记录成功][device][成功]: ",res)
                        /**
                         * 刷新页面
                         */
                        this.onLoad()
                      },
                      fail: err => {
                        console.log("[更新记录失败][device][失败]: ",err)
                      }
                    })  
                  }
                },
                fail: err => {
                  console.log("[查询数据库集合][device][失败]: ",err)
                }
              })
            }
          }
          else
          console.log("[qrcode parse json(dev_name) false]")
        }
      },
      fail: err => {
        console.log(err)
      }
    })
  },
  /**
   *  刷新记录按钮 
   */
  req_event_button: function(e){
    console.log("[req_event_button]")
    
    /**
     * 刷新页面
     */
    this.onLoad()
  },
  /**
   * 保存图片
   */
  save_image_button: function(e) {
    console.log("[save_image_button]")

    wx.saveImageToPhotosAlbum({
      filePath: this.data.display_image,
      success(res){
        console.log("[save_image_button][保存图片成功]")
        wx.showToast({
          title: '保存成功',
        }) 
      },
      fail(err){
        console.log("[save_image_button][保存图片失败]")
      }
    })
  },
  /**
   * 删除图片
   */
  dele_image_button: function(e){
    console.log("[dele_image_button]")

    wx.showModal({
      title: '提示',
      content: '是否要删除图片',
      success: res => {
        if (res.confirm) {
          console.log('用户点击确定')

          const db = wx.cloud.database()
          db.collection('event').doc(event_now_id).remove({
            success: res => {
              console.log(res)
              console.log("[dele_image_button][删除日志成功]")
              wx.showToast({
                title: '删除成功',
              })

              console.log("[current_file_name]",current_file_name)
              /**
               * 删除文件
               */
              wx.cloud.deleteFile({
                fileList: ["cloud://lononcam-7gzsafx250a9853b.6c6f-lononcam-7gzsafx250a9853b-1304353880/" + 'image/' + current_file_name]
              }).then(res => {
                console.log("[删除文件成功]",res.fileList)
              }).catch(error => {
                console.log(error)
              })

              /**
               * 刷新页面（有监听无需执行）
               */
              // this.onLoad()
            },
            fail: err => {
              console.log("[dele_image_button][删除日志失败]: ",err)
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
    return
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    console.log("[分享来自]", res.from)

    if(res.from != "button")
    {
      return {
        path: '/pages/image/image',
        imageUrl: "../../images/lononCam.png"
      }
    }

    if(!obj.time_stamp || !getApp().globalData.deviceName)
    {
      wx.showToast({
        title: '无效分享'
      }) 
      return {
        title: '照片分享',
        path: '/pages/image/image',
        imageUrl: "../../images/lononCam.png"
      }
    }

    console.log("[cover]","cloud://lononcam-7gzsafx250a9853b.6c6f-lononcam-7gzsafx250a9853b-1304353880/" + 'image/' + obj.time_stamp)

    return {
      title: '照片分享',
      path: '/pages/Share/Share?image=' + current_file_name + '&device_name=' + getApp().globalData.deviceName,
      imageUrl: "cloud://lononcam-7gzsafx250a9853b.6c6f-lononcam-7gzsafx250a9853b-1304353880/" + 'image/' + current_file_name
    }
  },
  /**
   * 拍摄照片按钮
   */
  take_photo_button: function(e){
    console.log("[take_photo_button]")

    wx.showToast({
      icon: 'loading',
      title: '正在发送',
    })
    wx.cloud.callFunction({ //推送数据  到指定主题
      name: 'iothub-publish',
      data: {
        SecretId: app.globalData.secretId,
        SecretKey: app.globalData.secretKey,
        ProductId: app.globalData.productId,
        DeviceName: app.globalData.deviceName,
        Topic: app.globalData.productId + "/" + app.globalData.deviceName + "/control",
        Payload: "{\"cmd\":\"take_photo\"}",
      },
      success: res => {
        wx.showToast({
          title: '调用成功',
        })
        console.log("res:", res)
      },
      fail: err => {

        console.error('[云函数] [iotexplorer] 调用失败：', err)
        if(err.errCode == -404011)
        {
          wx.showToast({
            icon: 'none',
            title: '设备未订阅Topic',
          })
        }
        else
        {
          wx.showToast({
            icon: 'none',
            title: '调用失败',
          })
        }
      }
    }) 
  },
  /**
   * 重启设备
   */
  reset_button: function(e){
    console.log("[reset_button]")

    wx.showToast({
      icon: 'loading',
      title: '正在发送',
    })
    wx.cloud.callFunction({ //推送数据  到指定主题
      name: 'iothub-publish',
      data: {
        SecretId: app.globalData.secretId,
        SecretKey: app.globalData.secretKey,
        ProductId: app.globalData.productId,
        DeviceName: app.globalData.deviceName,
        Topic: app.globalData.productId + "/" + app.globalData.deviceName + "/control",
        Payload: "{\"cmd\":\"reset\"}",
      },
      success: res => {
        wx.showToast({
          title: '调用成功',
        })
        console.log("res:", res)
      },
      fail: err => {

        console.error('[云函数] [iotexplorer] 调用失败：', err)
        if(err.errCode == -404011)
        {
          wx.showToast({
            icon: 'none',
            title: '设备未订阅Topic',
          })
        }
        else
        {
          wx.showToast({
            icon: 'none',
            title: '调用失败',
          })
        }
      }
    }) 
  },
  /**
   * 删除设备
   */
  dele_dev_button: function(e){
    console.log("[dele_dev_button]")

    wx.showModal({
      title: '提示',
      content: '是否要删除设备',
      success: res => {
        if (res.confirm) {
          console.log('用户点击确定')
          
          /** 
           * 读取数据库设备名称信息
           */
          const db = wx.cloud.database()
          /**
           * 查询当前用户的 device
           */
          db.collection('device').where({
            _openid: this.data.openid
          }).get({
            success: res => {
              console.log("[查询记录成功][device][成功]: ",res)
              
              if(res.data[0].device_name.includes(getApp().globalData.deviceName))
              {
                console.log("[待删除设备][存在]",getApp().globalData.deviceName)

                var dele_dev = []
                dele_dev = res.data[0].device_name

                for(var i = 0;i < dele_dev.length;i ++)
                {
                  if(dele_dev[i] == getApp().globalData.deviceName)
                  {
                    console.log("[找到要删除的设备]",i)
                    dele_dev.splice(i,1)
                    break
                  }
                }
                console.log("[dele_dev]",dele_dev)
                
                db.collection('device').doc(res.data[0]._id).update({
                  data: {
                    device_name: dele_dev
                  },
                  success: res => {
                    console.log("[删除设备成功][device][成功]: ",res)
                    now_set_dev = 0
                    this.onLoad()
                  },
                  fail: err => {
                    console.log("[删除设备失败][device][失败]: ",err)
                    now_set_dev = 0
                    this.onLoad()
                  }
                })  
              }
              else
              {
                console.log("[待删除设备][不存在]",getApp().globalData.deviceName)
              }
            },
            fail: err => {
              console.log("[查询记录失败][device][失败]: ",err)
            }
          })
        }
      }
    })
  },
  /**
   * 监听数据库
   */
  listen_database_change: function(e){
    console.log("[listen_database_change]")

    const db = wx.cloud.database()
    watcher = db.collection('event')
      .where({
        event: "photo",
        device_name: getApp().globalData.deviceName
      })
      .watch({
        onChange: res => {
          lis_fig = true
          console.log('[onChange][res.type]', res.type)
          if(res.type != "init")
          {
            console.log('[onChange][res.type != "init"]')
            this.onLoad()
          }
        },
        onError: function(err) {
          console.error('the watch closed because of error', err)
          lis_fig = false
        }
      })
  },
  /**
   * 导航栏变化
   */
  handleChange ({ detail }) {
    console.log(detail)
    this.setData({
        current: detail.key
    });
    switch(detail.key)
    {
      case "camera_key":
        this.setData({
          camera_page_sw: true,
          device_page_sw: false,
          setup_page_sw: false
      });
      break
      case "device_key":
        this.setData({
          camera_page_sw: false,
          device_page_sw: true,
          setup_page_sw: false
      });

      break
      case "setup_key":
        this.setData({
          camera_page_sw: false,
          device_page_sw: false,
          setup_page_sw: true
      });
      break
    }
  },
  /**
   * 点击设备列表
   */
  device_click_event(e){
    console.log("[device_click_event]",e.currentTarget.dataset.index)

    wx.showToast({
      title: '切换设备',
    })
    this.setData({
      current: 'camera_key',
      camera_page_sw: true,
      device_page_sw: false,
      setup_page_sw: false
    })
    now_set_dev = e.currentTarget.dataset.index
    getApp().globalData.deviceName = lis[e.currentTarget.dataset.index]
    this.onLoad()
  },
  /**
   * 更多设置
   */
  moer_setup_button(e){
    console.log("[moer_setup_button]")
    this.setData({
      showRight1: !this.data.showRight1
    })
  },
  /**
   * 关闭更多设置
   */
  toggleRight1(e){
    console.log("[toggleRight1]")
    this.setData({
      showRight1: !this.data.showRight1
    })
  },
  /**
   * 获取设备状态
   */
  get_device_state_button(e){
    console.log("[get_device_state_button]")

    wx.showToast({
      icon: 'loading',
      title: '正在请求',
    })
    wx.cloud.callFunction({ //推送数据  到指定主题
      name: 'iothub_get_state',
      data: {
        SecretId: app.globalData.secretId,
        SecretKey: app.globalData.secretKey,
        ProductId: app.globalData.productId,
        DeviceName: app.globalData.deviceName,
      },
      success: res => {
        if(res.result.Online == 1)
        {
          wx.showToast({
            title: '设备在线',
          })
        }
        else
        {
          wx.showToast({
            title: '设备不在线',
          })
        }
        
        console.log("res:", res)
      },
      fail: err => {
        console.error('[云函数] [iotexplorer] 调用失败：', err)
      }
    }) 
  },
  /**
   * 固件升级
   */
  get_device_ota_button(e){
    console.log("[get_device_ota_button]")
    
    wx.showToast({
      icon: 'loading',
      title: '正在发送',
    })
    wx.cloud.callFunction({ //推送数据  到指定主题
      name: 'iothub-publish',
      data: {
        SecretId: app.globalData.secretId,
        SecretKey: app.globalData.secretKey,
        ProductId: app.globalData.productId,
        DeviceName: app.globalData.deviceName,
        Topic: app.globalData.productId + "/" + app.globalData.deviceName + "/control",
        Payload: "{\"cmd\":\"ota\"}",
      },
      success: res => {
        wx.showToast({
          title: '调用成功',
        })
        console.log("res:", res)
      },
      fail: err => {

        console.error('[云函数] [iotexplorer] 调用失败：', err)
        if(err.errCode == -404011)
        {
          wx.showToast({
            icon: 'none',
            title: '设备未订阅Topic',
          })
        }
        else
        {
          wx.showToast({
            icon: 'none',
            title: '调用失败',
          })
        }
      }
    })     
  }
})


// miniprogram/pages/setnetwork/setnetwork.js
var ssid_string,password_string

Page({
  /**
   * 页面的初始数据
   */
  data: {

  },
  /**
   * 发送ssid信息按钮事件
   */
  sendssidinfo() {
    wx.showToast({
      icon: 'loading',
      title: '发送',
    })

    wx.request({
      url: 'http://192.168.4.1/echo', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        passwd: password_string,
        ssid: ssid_string
      },
      timeout: 30000,
      header: {
        'content-type': 'application/json',
        'x-access-token': 'TdVmqLfSXsUPkCDNYygsMcj8mgaBuYLD'
      },
      success (res) {
        console.log(res.data)
        wx.showToast({
          icon: 'success',
          title: '发送成功',
        })
      },
      fail: (err) => {
        console.log(err);
        wx.showToast({
          icon: 'none',
          title: '发送失败',
        })
      },
    })
  },

  /**
   * ssid输入变化事件
   */
  ssid_input(e) {
    ssid_string = e.detail.value
  },
  /**
   * password输入变化事件
   */
  password_input(e) {
    password_string = e.detail.value
  },
















  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})